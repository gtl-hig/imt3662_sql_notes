
package no.hig.imt3662.notes;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * Represents a single note.
 *
 */
public class Note {
	
	long id;
	String title;
	String body;
	
	private Note() {}
	
	public Note(final String title, final String body) {
		this.title = title;
		this.body = body;
	}
	
	public void save(DatabaseHelper dbHelper) {
		final ContentValues values = new ContentValues();
		values.put(TITLE, this.title);
		values.put(BODY, this.body);
		
		final SQLiteDatabase db = dbHelper.getReadableDatabase();
		this.id = db.insert(NOTES_TABLE_NAME, null, values);
		db.close();
	}
	
	public static Note[] getAll(final DatabaseHelper dbHelper) {
		 final List<Note> notes = new ArrayList<Note>();
		 final SQLiteDatabase db = dbHelper.getWritableDatabase();
		 final Cursor c = db.query(NOTES_TABLE_NAME,
				 new String[] { ID, TITLE, BODY}, null, null, null, null, null);
		 // make sure you start from the first item
		 c.moveToFirst();
		 while (!c.isAfterLast()) {
			 final Note note = cursorToNote(c);
		     notes.add(note);
		     c.moveToNext();
		 }
		 // Make sure to close the cursor
		 c.close();
		 return notes.toArray(new Note[notes.size()]);
	}
	
	public static Note cursorToNote(Cursor c) {
		final Note note = new Note();
		note.setTitle(c.getString(c.getColumnIndex(TITLE)));
		note.setBody(c.getString(c.getColumnIndex(BODY)));
		return note;
	}

	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}
	
	public String toString() {
		return this.title;
	}
	
	
	
	public static final String NOTES_TABLE_NAME = "notes";
	// column names
	static final String ID = "id"; // 
	static final String TITLE = "title";
	static final String BODY = "body";
	// SQL statement to create our table
	public static final String NOTES_CREATE_TABLE = "CREATE TABLE " + Note.NOTES_TABLE_NAME + " ("
							+ Note.ID + " INTEGER PRIMARY KEY,"
							+ Note.TITLE + " TEXT,"
							+ Note.BODY + " TEXT"
							+ ");";

	
}
